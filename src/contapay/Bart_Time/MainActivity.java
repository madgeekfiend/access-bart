package contapay.Bart_Time;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import contapay.Bart_Time.Bart.BartApi;
import contapay.Bart_Time.Models.BartStation;
import contapay.Bart_Time.Models.BartTrain;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainActivity extends Activity implements AdapterView.OnItemSelectedListener {

    private ListView mListTrains;
    private List<BartStation> mBartStations = new ArrayList<BartStation>();
    private Spinner spinner;
    private BartStation mCurrentSelectedStation;
    private BartApi.Direction mCurrentDirection;

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        mCurrentSelectedStation = (BartStation)spinner.getSelectedItem();
        populateTrainList();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        spinner = (Spinner)findViewById(R.id.spinner_stations);
        mCurrentDirection = BartApi.Direction.ALL;

        BartApi ba = new BartApi();
        Map<String, String> mStations = ba.getStations(getResources().getString(R.string.bart_key));

        // Move list of stations to
        for(Map.Entry<String, String> entry : mStations.entrySet() )
        {
            BartStation bs = new BartStation();
            bs.setName(entry.getKey());
            bs.setAbbreviation(entry.getValue());
            mBartStations.add(bs);
        }

        //populate spinner
        ArrayAdapter spinnerArrayAdapter = new ArrayAdapter(this,
                android.R.layout.simple_spinner_item, mBartStations);
        spinner.setAdapter(spinnerArrayAdapter);
        spinner.setOnItemSelectedListener(this);

        // Assign the vars
        mListTrains = (ListView)findViewById(R.id.listView_trains);
    }

    private void populateTrainList()
    {
        // Only populate if both things are filled out
        if ( mCurrentSelectedStation.getName().length() > 0 )
        {
            // Take the current selected station and get all trains for that station
            // Get the current station
            BartApi api = new BartApi();
            List<BartTrain> trains = api.getTrainsForStation(mCurrentSelectedStation.getAbbreviation().toLowerCase(),getResources().getString(R.string.bart_key),mCurrentDirection);

            // Now that I got a list of trains let's populate the control
            mListTrains.setAdapter(new TrainsListAdapter(this,trains));
        }
    }
}
