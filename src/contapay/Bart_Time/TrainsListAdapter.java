package contapay.Bart_Time;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import contapay.Bart_Time.Bart.BartApi;
import contapay.Bart_Time.Models.BartTrain;

import java.util.List;

/**
 * Created by Sam on 9/25/13.
 */
public class TrainsListAdapter extends ArrayAdapter<BartTrain> {
    private List<BartTrain> trains;
    private Context context;

    public TrainsListAdapter(Context context, List<BartTrain> trains)
    {
        super(context,R.layout.list_bart_train,trains);
        this.trains = trains;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_bart_train,parent,false);

        TextView trainName = (TextView)rowView.findViewById(R.id.train_name);
        TextView trainTime = (TextView)rowView.findViewById(R.id.text_time);
        SurfaceView trainColor = (SurfaceView)rowView.findViewById(R.id.surface_color);
        ImageView imageArrow = (ImageView)rowView.findViewById(R.id.image_arrow);
        TextView allowBikes = (TextView)rowView.findViewById(R.id.allow_bikes);

        BartTrain bt = trains.get(position);
        trainName.setText(bt.destination);
        trainTime.setText(String.valueOf(bt.eta));

        trainColor.setBackgroundColor(Color.parseColor(bt.hexcolor));
        imageArrow.setImageResource((bt.direction.equalsIgnoreCase("North"))?R.drawable.arrow_up:R.drawable.arrow_down);
        trainTime.setText(String.format("%s min",padLeft(String.valueOf(bt.eta),2,'0')));
        String bikesString = (bt.bikeFlag)?"Bikes Allowed":"Bikes Not Allowed";
        allowBikes.setText(String.format("%s (%d)",bikesString,bt.numberOfCars));

        return rowView;
    }

    private String padLeft(String str, int totalLength, char padChar)
    {
        String padding = "";
        for(int i = 0; i < (totalLength - str.length()); i++)
        {
            padding += padChar;
        }
        return padding + str;
    }
}
