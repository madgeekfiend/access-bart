package contapay.Bart_Time.Bart;

import android.util.Log;
import contapay.Bart_Time.Models.BartTrain;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.*;

/**
 * Created by Sam on 9/24/13.
 */
public class BartApi {

    public static enum Direction { ALL, NORTH, SOUTH };

    public static final String BART_API_URL = "http://api.bart.gov/api/etd.aspx?";
    public static final String BART_API_STATIONS_URL = "http://api.bart.gov/api/stn.aspx?";

    private static final String LOG_TAG = "BartApi";

    public BartApi()
    {
        Log.i(LOG_TAG, String.format("Initializing with URL: %s", BART_API_URL));
    }

    public List<BartTrain> getTrainsForStation(String stationName, String bartKey, Direction direction)
    {
        List<BartTrain> trains = new LinkedList<BartTrain>();
        List<NameValuePair> params = new LinkedList<NameValuePair>();

        // Build parameter list
        params.add(new BasicNameValuePair("cmd","etd"));
        params.add(new BasicNameValuePair("orig",stationName));
        params.add(new BasicNameValuePair("key",bartKey));

        String url = BART_API_URL + URLEncodedUtils.format(params,"utf-8");

        try {
            Document trainsDoc = makeUrlCall(url);
            trainsDoc.getDocumentElement().normalize();

            NodeList nodeList = trainsDoc.getElementsByTagName("etd");
            for (int temp = 0; temp < nodeList.getLength();temp++)
            {
                Node node = nodeList.item(temp);

                if ( node.getNodeType() == Node.ELEMENT_NODE)
                {
                    Element ele = (Element)node;
                    Node estimate = ele.getElementsByTagName("estimate").item(0);
                    if ( estimate.getNodeType() == Node.ELEMENT_NODE)
                    {
                        try {
                            Element est = (Element)estimate;
                            BartTrain newTrain = new BartTrain();
                            newTrain.destination = ele.getElementsByTagName("destination").item(0).getTextContent();
                            newTrain.eta =  Integer.parseInt(est.getElementsByTagName("minutes").item(0).getTextContent());
                            newTrain.numberOfCars = Integer.parseInt(est.getElementsByTagName("length").item(0).getTextContent());
                            newTrain.hexcolor = est.getElementsByTagName("hexcolor").item(0).getTextContent();
                            newTrain.direction = est.getElementsByTagName("direction").item(0).getTextContent();
                            newTrain.bikeFlag = est.getElementsByTagName("bikeflag").item(0).getTextContent().equalsIgnoreCase("1");
                            trains.add(newTrain);
                        }
                        catch (NumberFormatException e)
                        {
                            Log.d("BartApi",e.getMessage());
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }

        return trains;
    }

    public Map<String, String> getStations(String bartKey)
    {
        List<NameValuePair> params = new LinkedList<NameValuePair>();
        Map<String, String> stations = new Hashtable<String, String>();

        // Build params
        params.add(new BasicNameValuePair("cmd","stns"));
        params.add(new BasicNameValuePair("key",bartKey));

        String paramString = URLEncodedUtils.format(params,"utf-8");

        // Get the XML document
        try {
            Document doc = makeUrlCall(BART_API_STATIONS_URL + paramString);
            // Now parse the xml
            doc.getDocumentElement().normalize();

            NodeList nodeList =  doc.getElementsByTagName("station");
            for(int temp = 0; temp < nodeList.getLength(); temp++)
            {
                Node nNode = nodeList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE)
                {
                    Element element = (Element)nNode;

                    stations.put(element.getElementsByTagName("name").item(0).getTextContent(),element.getElementsByTagName("abbr").item(0).getTextContent());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }

        return stations;
    }

    /*
    Got this code snippet from
    http://stackoverflow.com/questions/3395154/android-read-an-xml-file-with-http-get
     */
    private Document makeUrlCall(String url) throws IOException, ParserConfigurationException, SAXException {
        HttpGet uri = new HttpGet(url);
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse resp = client.execute(uri);

        StatusLine status = resp.getStatusLine();
        if (status.getStatusCode() != 200 )
        {
            Log.d(LOG_TAG,"HTTP error, invalid status code: " + String.valueOf(resp.getStatusLine()) );
        }

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.parse(resp.getEntity().getContent());
    }
}
