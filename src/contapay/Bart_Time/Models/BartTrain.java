package contapay.Bart_Time.Models;

/**
 * Created by Sam on 9/25/13.
 */
public class BartTrain {
    public String destination;
    public int eta;
    public String direction;
    public int numberOfCars;
    public boolean bikeFlag;
    public String hexcolor;
}
